from page_objects import PageObject, PageElement

class LoginPage(PageObject):
    username_input=PageElement(id_="myid")
    password_input=PageElement(id_="mypass")

    def check_page(self):
        return "Sign In" in self.w.title

    def login(self,mainpage,username,password):
        self.username_input.send_keys(username)
        self.password_input.send_keys(password)
        self.password_input.submit()
        return mainpage.check_page()

from page_objects import PageObject, PageElement

from  selenium.webdriver.support.ui import WebDriverWait

from selenium.webdriver.support import expected_conditions as EC

class MainPage(PageObject):

    logout_link = PageElement(link_text="Logout")

    say_something = PageElement(css="button.btn.btn-lg.btn-success")



    def check_page(self):
        return "Logged In" in self.w.title

    def click_logout(self, mainpage):
        self.logout_link.click()
        return mainpage.check_page()

    def click_say_something(self, mainpage):
        self.say_something.click()

        alert = self.w.switch_to_alert()

       # try:
          #  alert = WebDriverWait(self.w,10).until(EC.alert_is_present)
       # except TimeoutError:
         #  print("No alert yet")

        alert.accept()

        return mainpage.check_page()



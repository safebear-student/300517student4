from page_objects import PageObject, PageElement

class FramePage(PageObject):
    def check_page(self):
        return "Frame Page" in self.w.title

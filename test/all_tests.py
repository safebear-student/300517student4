import base_test

class TestCases (base_test.BaseTest):
    def test_01_login_page_login(self):
        #step 1: click on login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)
        assert self.loginpage.login(self.mainpage,"testuser","testing")
        assert self.mainpage.click_logout(self.welcomepage)
        assert self.welcomepage.click_login(self.loginpage)
        assert self.loginpage.login(self.mainpage,"testuser","testing")
        assert self.mainpage.click_say_something(self.mainpage)
